set nocompatible
filetype off

let g:TerminusCursorShape=0

if executable("ag")
  let g:CtrlSpaceGlobCommand = 'ag -l --nocolor -g ""'
  set grepprg=ag\ --nogroup\ --nocolor
  " Use ag in CtrlP for listing files. Lightning fast and respects
  let g:ctrlp_user_command = 'ag %s -l --nocolor -g ""'
endif


let g:polyglot_disabled = ['tex', 'latex'] " disabled since this is handled by vimtex

set rtp+=~/.vim/bundle/Vundle.vim

"Plugins go between the #begin and #end, below.

call vundle#begin()
Plugin 'VundleVim/Vundle.vim'
Plugin 'tpope/vim-sensible'
Plugin 'derekwyatt/vim-scala'
Plugin 'tpope/vim-sleuth'
" Git integration
Plugin 'tpope/vim-fugitive'
Plugin 'vim-ctrlspace/vim-ctrlspace'
Plugin 'scrooloose/nerdtree'
Plugin 'benmills/vimux'
Plugin 'ctrlpvim/ctrlp.vim'
Plugin 'vim-scripts/a.vim'
Plugin 'vim-scripts/DoxygenToolkit.vim'
Plugin 'vim-scripts/guicolorscheme.vim'
Plugin 'vim-scripts/vim-indent-object'
Plugin 'vim-scripts/jslint.vim'
"Plugin 'davidhalter/jedi-vim'
Plugin 'vim-latex/vim-latex'
Plugin 'xolox/vim-notes'
Plugin 'vim-scripts/netrw.vim'
Plugin 'xolox/vim-misc'
Plugin 'trammell/subscripts.vim'
Plugin 'iitaku/clewn-mirror'
Plugin 'wincent/terminus'
Plugin 'inkarkat/SmartCase'
Plugin 'kchmck/vim-coffee-script'
Plugin 'jlanzarotta/bufexplorer'
Plugin 'jelera/vim-javascript-syntax'
Plugin 'mtscout6/vim-cjsx'
Plugin 'Chiel92/vim-autoformat'
Plugin 'triglav/vim-visual-increment'
Plugin 'haya14busa/git-mergetool-vimdiff-wrapper'
Plugin 'vim-scripts/AutoAlign'
Plugin 'vim-scripts/lua.vim'
Plugin 'pangloss/vim-javascript'
Plugin 'wookiehangover/jshint.vim'
Plugin 'lervag/vimtex'
Plugin 'vim-scripts/gdbvim'
Plugin 'sheerun/vim-polyglot'
Plugin 'gisraptor/vim-lilypond-integrator'
Plugin 'psf/black'
Plugin 'igankevich/mesonic'
Bundle 'powerline/powerline', {'rtp': 'powerline/bindings/vim/'}
Plugin 'rust-lang/rust.vim'
Plugin 'tarekbecker/vim-yaml-formatter'
Plugin 'cstrahan/vim-capnp'
Plugin 'projectfluent/fluent.vim'
Plugin 'dense-analysis/ale'
Plugin 'rhysd/vim-clang-format'
Plugin 'ntpeters/vim-better-whitespace'
Plugin 'jacoborus/tender.vim'
Plugin 'fugalh/desert.vim'

Plugin 'whiteinge/diffconflicts'
Plugin 'https://github.com/yegappan/lsp'

call vundle#end()

filetype plugin indent on
let g:jellybeans_overrides = {
\  'Cursor': { 'guibg': 'ff00ee', 'guifg': '00ff11' }
\}
let g:notes_directories = ['~/Notes']

let mapleader="\\"
set mouse=a
set ttyfast

set ttymouse=xterm

set hidden

syntax enable

let g:CtrlSpaceSearchTiming = 500
let g:CtrlSpaceUseTabline = 1

set number

set expandtab
set tabstop=4
set shiftwidth=4
set softtabstop=4

set hlsearch
set ttymouse=sgr

autocmd QuickFixCmdPost *grep* cwindow

set backup
set backupdir=~/.vimbkp
set dir=~/.vimswap
set autoread

command! -range -nargs=0 -bar JsonTool <line1>,<line2>!python -m json.tool

" Set to auto read when a file is changed from the outside
set autoread

" Set 7 lines to the cursor - when moving vertically using j/k
set so=7

" Turn on the WiLd menu
set wildmenu

" Ignore compiled files
set wildignore=*.o,*~,*.pyc

"Always show current position
set ruler

" Height of the command bar
set cmdheight=2

" A buffer becomes hidden when it is abandoned
set hid

" Configure backspace so it acts as it should act
set backspace=eol,start,indent
set whichwrap+=<,>,h,l

" Ignore case when searching
set ignorecase

" When searching try to be smart about cases
set smartcase

" Highlight search results
set hlsearch

" Makes search act like search in modern browsers
set incsearch

" For regular expressions turn magic on
set magic

" Show matching brackets when text indicator is over them
set showmatch
" How many tenths of a second to blink when matching brackets
set mat=2

" No annoying sound on errors
set noerrorbells
set novisualbell
set t_vb=
set tm=500

" Set utf8 as standard encoding and en_US as the standard language
set encoding=utf8

" Use Unix as the standard file type
set ffs=unix,dos,mac

set ai "Auto indent
set si "Smart indent
set wrap "Wrap lines

""""""""""""""""""""""""""""""
" => Visual mode related
""""""""""""""""""""""""""""""
" Visual mode pressing * or # searches for the current selection
" Super useful! From an idea by Michael Naumann
vnoremap <silent> * :call VisualSelection('f')<CR>
vnoremap <silent> # :call VisualSelection('b')<CR>

" Treat long lines as break lines (useful when moving around in them)
map j gj
map k gk

" Smart way to move between windows
map <C-j> <C-W>j
map <C-k> <C-W>k
map <C-h> <C-W>h
map <C-l> <C-W>l
nmap <silent> <A-h> :tabp<CR>
nmap <silent> <A-l> :tabn<CR>
""""""""""""""""""""""""""""""
" => Status line
""""""""""""""""""""""""""""""
" Always show the status line
set laststatus=2

" Format the status line
set statusline=\ %{HasPaste()}%F%m%r%h\ %w\ \ CWD:\ %r%{getcwd()}%h\ \ \ Line:\ %l

function! HasPaste()
    if &paste
        return 'PASTE MODE  '
    en
    return ''
endfunction

let g:ctrlspace_use_tabline=1
nmap <silent> <A-Up> :wincmd k<CR>
nmap <silent> <A-Down> :wincmd j<CR>
nmap <silent> <A-Left> :wincmd h<CR>
nmap <silent> <A-Right> :wincmd l<CR>

set nrformats=alpha,hex
"let g:ctrlp_map = '<c-q>'
"let g:ctrlp_cmd = 'CtrlQ'

set tags=tags;/

"Note: This option must be set in .vimrc(_vimrc).  NOT IN .gvimrc(_gvimrc)!
" Disable AutoComplPop.
let g:acp_enableAtStartup = 0

function! DoPrettyXML()
  " save the filetype so we can restore it later
  let l:origft = &ft
  set ft=
  " delete the xml header if it exists. This will
  " permit us to surround the document with fake tags
  " without creating invalid xml.
  1s/<?xml .*?>//e
  " insert fake tags around the entire document.
  " This will permit us to pretty-format excerpts of
  " XML that may contain multiple top-level elements.
  0put ='<PrettyXML>'
  $put ='</PrettyXML>'
  silent %!xmllint --format -
  " xmllint will insert an <?xml?> header. it's easy enough to delete
  " if you don't want it.
  " delete the fake tags
  2d
  $d
  " restore the 'normal' indentation, which is one extra level
  " too deep due to the extra tags we wrapped around the document.
  "silent %<
  " back to home
  1
  " restore the filetype
  exe "set ft=" . l:origft
endfunction
command! PrettyXML call DoPrettyXML()

" Sets how many lines of history VIM has to remember
set history=700

" Don't redraw while executing macros (good performance config)
set lazyredraw

" Colors of CtrlSpace for Solarized Dark
" (MacVim and Console Vim under iTerm2 with Solarized Dark theme)

" Based on Solarized TablineSel
hi CtrlSpaceSelected guifg=#586e75 guibg=#eee8d5 guisp=#839496 gui=reverse,bold ctermfg=10 ctermbg=7 cterm=reverse,bold

" Based on Solarized Tabline/TablineFill
" original Normal
" hi CtrlSpaceNormal   guifg=#839496 guibg=#073642 guisp=#839496 gui=NONE ctermfg=12 ctermbg=0 cterm=NONE
" tweaked Normal with darker background in Gui
hi CtrlSpaceNormal   guifg=#839496 guibg=#021B25 guisp=#839496 gui=NONE ctermfg=12 ctermbg=0 cterm=NONE

" Based on Title
hi CtrlSpaceSearch   guifg=#cb4b16 guibg=NONE gui=bold ctermfg=9 ctermbg=NONE term=bold cterm=bold

" Based on PmenuThumb
hi CtrlSpaceStatus   guifg=#839496 guibg=#002b36 gui=reverse term=reverse cterm=reverse ctermfg=12 ctermbg=8

let g:ctrlspace_use_tabline=1
filetype plugin on
let g:livepreview_previewer = 'okular'
autocmd FileType tex setlocal makeprg=latexmk\ -view=pdf
let g:vimtex_view_general_viewer = 'okular'
let g:vimtex_view_general_options = '--unique @pdf\#src:@line@tex'
let g:vimtex_view_general_options_latexmk = '--unique'

autocmd BufNewFile,BufRead *.{pybuild,pmodule} set syntax=python
autocmd BufNewFile,BufRead *.{pybuild,pmodule} set filetype=python

"autocmd Filetype ipynb nmap <silent><Leader>b :VimpyterInsertPythonBlock<CR>
"autocmd Filetype ipynb nmap <silent><Leader>j :VimpyterStartJupyter<CR>
"autocmd Filetype ipynb nmap <silent><Leader>n :VimpyterStartNteract<CR>

autocmd BufNewFile,BufRead *.tex set spell spelllang=en_GB
autocmd BufNewFile,BufRead *.txt set spell spelllang=en_GB

let g:ale_fixers = {'python': ['isort', 'black']}
let g:clang_format#code_style = "google"

let g:ale_completion_enabled = 0
let g:ale_echo_msg_format = '[%linter%/%code%] %s [%severity%]'
let g:ale_virtualtext_cursor = 0
let g:ale_rust_cargo_use_clippy = 1
let g:ale_disable_lsp = 1

colorscheme desert
let g:rustfmt_autosave = 1

autocmd VimEnter * call LspAddServer([#{
	\    name: 'clangd',
	\    filetype: ['c', 'cpp'],
    \    path: "clangd",
	\    args: ['--background-index']
	\  }])

" Rust language server
autocmd VimEnter * call LspAddServer([#{
	\    name: 'rustlang',
	\    filetype: ['rust'],
    \    path: "rust-analyzer",
	\    args: [],
	\    syncInit: v:true
	\  }])


" Rust language server
autocmd VimEnter * call LspAddServer([#{
	\    name: 'python',
	\    filetype: ['py'],
	\    path: "pylsp",
	\    args: [],
	\  }])


nnoremap <silent> <nowait> <C-]> :LspGotoDefinition<CR>
inoremap <expr> <TAB> pumvisible() ? "\<C-n>" : "\<TAB>"
inoremap <expr> <S-TAB> pumvisible() ? "\<C-p>" : "\<TAB>"
